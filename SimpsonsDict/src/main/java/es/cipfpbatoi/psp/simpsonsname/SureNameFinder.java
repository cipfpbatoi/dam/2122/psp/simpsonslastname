package es.cipfpbatoi.psp.simpsonsname;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Scanner;

public class SureNameFinder {

    private final String WORD_SEPARATOR = "=>";

    private HashMap<String,String> diccionario = new HashMap();

    SureNameFinder(){

        //Familia Simpson
        diccionario.put("Homer","Jay Simpson");
        diccionario.put("Marge","Simpson");
        diccionario.put("Bart","Simpson");
        diccionario.put("Lisa","Simpson");
        diccionario.put("Maggie","Simpson");
        diccionario.put("Abraham","Simpson");

        //Central Nuclear
        diccionario.put("Lenny","Leonard");

    }

    public SureNameFinder(File dictFile)
    {
        try (FileReader fr = new FileReader(dictFile);){

            Scanner sc = new Scanner(fr);

            String line;

            while (sc.hasNextLine()){

                line = sc.nextLine();
                String[] words = line.split("=>");

                diccionario.put(words[0],words[1]);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    String getLastName(String name) throws LastNameNotFoundException
    {
        if (!diccionario.containsKey(name)){
            throw new LastNameNotFoundException(name);
        }
        return diccionario.get(name);
    }


}
