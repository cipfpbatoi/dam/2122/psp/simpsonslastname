package es.cipfpbatoi.psp.simpsonsname;

import java.io.File;
import java.io.PrintStream;
import java.util.Locale;
import java.util.Scanner;

public class SimpsonsDict {

    private static final String DESCONOCIDO = "desconocido";
    public static final String STOP = "stop";

    private static final int EXIT_OK = 0;
    private static final int EXIT_ERROR_NAME_NOT_FOUND = 1;

    private static final int EXIT_ERROR_UNKNOWN = 10;
    private static final int EXIT_ERROR_WRONG_ARGS = 11;
    private static final int EXIT_ERROR_DICT_FILE_NOT_EXIST = 12;

    private static final String COMMAND_LINE_HELP = "-h";
    private static final String COMMAND_LINE_DICT = "-d";

    private static final String JAR_RUN_HELP = "java -jar SimpsonsDic.jar";


    public static void main(String[] args) {

        if (args.length == 0 || args.length == 1){

            SureNameFinder sf = new SureNameFinder();

            // Modo Interactivo sin diccionario
            if ( args.length == 0){

                EjecucionInteractiva(sf);

                // Modo comando ayuda
            } else if (args[0].equals(COMMAND_LINE_HELP))  {

                ImprimirInstruccionesDeUso(System.out);
                System.exit(EXIT_OK);

            } else {

                try{
                    EjecucionModoComando(sf,args[0]);
                } catch (Exception e){
                    System.err.println("Hubo un error desconocido llamando el programa");
                    System.err.println(e.getLocalizedMessage());
                    System.exit(EXIT_ERROR_UNKNOWN);
                }
            }


        } else if ( args.length == 2 || args.length == 3 ) {

            String diccionarioTXT = null;
            String nombre = null;

            // ¿where is the -d?
            int minusDIndex = -1;

            for (int i = 0; i< args.length -1 ; i++){
                if (args[i].equals(COMMAND_LINE_DICT)){
                    minusDIndex = i;
                    break;
                }
            }

            if ( minusDIndex == -1 ){
                System.err.println("Parámetros Incorrectos");
                ImprimirInstruccionesDeUso(System.err);
                System.exit(11);
            }

            if ( minusDIndex == 0){
                diccionarioTXT = args[1];
            }

            if ( args.length == 3)
            {
                if ( minusDIndex == 0){
                    nombre = args[2];
                } else {
                    nombre = args[0];
                    diccionarioTXT = args[2];
                }
            }

            File diccionario = new File(diccionarioTXT);

            if (!diccionario.exists() || !diccionario.isFile()){
                System.err.println("El diccionario no existe: " + diccionario.getAbsolutePath());
                ImprimirInstruccionesDeUso(System.err);
                System.exit(EXIT_ERROR_DICT_FILE_NOT_EXIST);
            }

            SureNameFinder sf = new SureNameFinder(diccionario);

            // Modo Interactivo sin diccionario
            if ( args.length == 2){

                EjecucionInteractiva(sf);

                // Modo comando sin diccionario
            } else {

                try{
                    EjecucionModoComando(sf,nombre);
                } catch (Exception e){
                    System.err.println("Hubo un error desconocido llamando el programa");
                    System.err.println(e.getLocalizedMessage());
                    System.exit(EXIT_ERROR_UNKNOWN);
                }
            }

        } else {
            System.err.println("Número de parámetros Incorrecto");
            ImprimirInstruccionesDeUso(System.err);
            System.exit(EXIT_ERROR_WRONG_ARGS);
        }

    }

    private static void EjecucionInteractiva(SureNameFinder nameFinder)
    {
        Scanner sc = new Scanner(System.in);

        String linea;

        while ( !( linea = sc.nextLine()).toLowerCase().equals(STOP) )
        {
            try{
                System.out.println(nameFinder.getLastName(linea));
            } catch (LastNameNotFoundException e)
            {
                System.err.println(DESCONOCIDO);
            }

        }

        System.exit(EXIT_OK);
    }

    private static void EjecucionModoComando(SureNameFinder nameFinder,String nombre)
    {
        try {
            System.out.println(nameFinder.getLastName(nombre));
        } catch (LastNameNotFoundException e)
        {
            System.err.println(DESCONOCIDO);
            System.exit(EXIT_ERROR_NAME_NOT_FOUND);
        }
        System.exit(EXIT_OK);
    }

    private static void ImprimirInstruccionesDeUso(PrintStream ps)
    {
        ps.println("============== Simpsons'Dict ==============");
        ps.println();
        ps.println("Este programa es un diccionario que dado el nombre de un");
        ps.println("personaje de la serie Los Simpson, devuelve su apellido");
        ps.println("en caso de que no lo tenga, escribirá por la salida de error ");
        ps.println(DESCONOCIDO);
        ps.println();
        ps.println("Tiene diversas formas de ejecución:");
        ps.println();
        ps.printf("1. Obtener ayuda: %s %s",JAR_RUN_HELP,COMMAND_LINE_HELP);
        ps.println();
        ps.printf("\tImprime la ayuda por la salida estándar y sale con código: %d", EXIT_OK);
        ps.println();ps.println();
        //Modo Interactivo
        ps.printf("2. Modo Interactivo: %s [-d <diccionario>]",JAR_RUN_HELP);
        ps.println();
        ps.println("\tSe ejecuta en modo interactivo y opcionalmente se le puede pasar");
        ps.println("\tun diccionario para que lo utilice en lugar del interno");
        ps.println("\t> El formato del diccionario es: <palabra>=><traduccion> <");
        ps.println("\tEjemplo del contenido: Homer=>Simpson");
        ps.println("\tAl ejecutarlo en modo interactivo el programa espera la entrada del");
        ps.println("\tusuario y cuando pulsa enter, lee la entrada, comprueba si tiene el valor ");
        ps.println("\ten el diccionario y entonces imprime en otra linea la traducción. En el caso");
        ps.println("\tde que tenga un error, devuelve por la salida de error estándar la palabra "+DESCONOCIDO+ ".");
        ps.println("\t> El programa finaliza cuando se escribe " + STOP + " <");
        ps.println();
        ps.printf("3. Modo Comando: %s [%s diccionario] <Nombre>",JAR_RUN_HELP,COMMAND_LINE_DICT);
        ps.println();
        ps.println("\tEn este caso, el programa toma como argumento el <Nombre> y  lo intenta");
        ps.println("\ttraducir utilizando el diccionario interno o el externo en función de ");
        ps.println("\tlos argumentos. Si encuentra la palabra, mostrará la traducción por la salida");
        ps.println("\testándar y finalizará con el código "+EXIT_OK+" .");
        ps.println("\tEn caso de que no encuentre la palabra, mostrará por la salida de error");
        ps.printf("\testándar el mensaje %s y finalizará con el código de error: %d",DESCONOCIDO,EXIT_ERROR_NAME_NOT_FOUND);
        ps.println();ps.println();
        ps.println("En cualquier otro caso o error el programa mostrará una descripción del error por");
        ps.println("la salida de error estándar y devolverá una salida de error");
        ps.println("A continuación se muestra una lista de las posibles salidas de error con sus codigos: ");
        ps.println();
        ps.printf("%d\tEXIT_ERROR_NAME_NOT_FOUND: No se ha encontrado el nombre en el diccionario",EXIT_ERROR_NAME_NOT_FOUND);
        ps.println();
        ps.println("\tEste error sólo se devuelve cuando se ejecuta en modo comando");
        ps.printf("%d\tEXIT_ERROR_UNKNOWN: Se ha producido un error desconocido",EXIT_ERROR_UNKNOWN);
        ps.println();
        ps.printf("%d\tEXIT_ERROR_WRONG_ARGS:Ha pasado algo al procesar los parámetros",EXIT_ERROR_WRONG_ARGS);
        ps.println();
        ps.printf("%d\tEXIT_ERROR_DICT_FILE_NOT_EXIST: El diccionario no existe o no es un fichero regular",EXIT_ERROR_DICT_FILE_NOT_EXIST);
        ps.println();


    }
}
