package es.cipfpbatoi.psp.simpsonsname;

public class LastNameNotFoundException extends Exception{

    public LastNameNotFoundException(String name)
    {
        super(name);
    }

}
