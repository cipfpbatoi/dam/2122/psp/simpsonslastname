import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * 4. Realiza la clase Ejercicio4 que acepte como parámetros una lista de nombres
 * y opcionalmente, un diccionario propio mediante mediante [-d diccionario.txt]
 * por simplicidad,  el diccionario, en caso de estar irá al principio.
 * La clase Ejercicio4 ha de ejecutar el proceso hijo por cada valor pasado como entrada
 * y mostrar una lista con los resultados por la salida:
 *
 * Ejemplo de ejecución: java -jar Ejercicio4.jar -d diccionario.txt Homer Lisa Marge
 * #Los resultados son a partir del diccionario.txt proporcionado en el ejercicio,
 * pero si se usa uno distinto, los resultados pueden ser distintos:
 * Homer Jay Simpson
 * desconocido
 * desconocido
 *
 */
public class Ejercicio4 {

    public static void main(String[] args) {

        String comando_texto = "java -jar SimpsonsDict.jar";

        List<String> parametrosHijo = new ArrayList<String>();

        for ( String param : comando_texto.split(" ")){
            parametrosHijo.add(param);
        }

        int primerParametro = 0;

        // Detectar si han pasado el diccionario
        if ( args.length > 1 && args[0].equals("-d")){
            parametrosHijo.add(args[0]);
            parametrosHijo.add(args[1]);
            primerParametro = 2;
        }

        for (int i = primerParametro; i<args.length; i++) {

            List<String> params = new ArrayList<>(parametrosHijo);
            params.add(args[i]);

            ProcessBuilder pb = new ProcessBuilder(params);

            try {
                Process procesoHijo = pb.start();

                Scanner procesoHijoScanner = new Scanner(procesoHijo.getInputStream());

                if (procesoHijo.waitFor() == 0){
                    System.out.printf("%s %s",args[i],procesoHijoScanner.nextLine());
                    System.out.println();
                } else {
                    System.out.println("Desconocido");
                }

            } catch (IOException|InterruptedException e) {
                e.printStackTrace();
            }

            params.remove(args[i]);


        }

    }

}
