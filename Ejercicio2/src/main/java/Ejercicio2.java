import java.io.IOException;
import java.util.Scanner;

/**
 * 2. Realiza la clase Ejercicio2 que ejecute el proceso hijo en modo
 * interactivo sin argumentos y que reenvíe al usuario todas las
 * entradas y salidas del proceso hijo mediante métodos de la
 * clase ProcessBuilder (0,5p)
 *
 */
public class Ejercicio2 {

    public static void main(String[] args) {

        String comando_texto = "java -jar SimpsonsDict.jar";

        ProcessBuilder pb = new ProcessBuilder(comando_texto.split(" "));

        pb.inheritIO();

        try {

            Process lenny = pb.start();
            lenny.waitFor();

        } catch (IOException |InterruptedException e){
            e.printStackTrace();
        }


    }

}
