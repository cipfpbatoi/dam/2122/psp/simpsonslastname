import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * 5. Realiza la clase Ejercicio5 que realice lo mismo que Ejercicio4
 * pero en lugar de mostrar el resultado por pantalla lo guarde en fichero
 * "resultados.txt" que en caso de que no exista se creará dentro del mismo
 * directorio del donde se encuentre el jar relativo a la clase
 */
public class Ejercicio5 {

    public static void main(String[] args) {

        String comando_texto = "java -jar SimpsonsDict.jar";

        List<String> parametrosHijo = new ArrayList<String>();

        for ( String param : comando_texto.split(" ")){
            parametrosHijo.add(param);
        }

        int primerParametro = 0;

        // Detectar si han pasado el diccionario
        if ( args.length > 1 && args[0].equals("-d")){
            parametrosHijo.add(args[0]);
            parametrosHijo.add(args[1]);
            primerParametro = 2;
        }

        File resultados = new File("resultados.txt");

        try (FileWriter fw = new FileWriter(resultados);
             BufferedWriter bw = new BufferedWriter(fw);
             PrintWriter pw = new PrintWriter(bw)){

            for (int i = primerParametro; i<args.length; i++) {

                List<String> params = new ArrayList<>(parametrosHijo);
                params.add(args[i]);

                ProcessBuilder pb = new ProcessBuilder(params);

                try  {
                    Process procesoHijo = pb.start();

                    Scanner procesoHijoScanner = new Scanner(procesoHijo.getInputStream());

                    if (procesoHijo.waitFor() == 0){
                        pw.printf("%s %s",args[i],procesoHijoScanner.nextLine());
                        pw.println();
                    } else {
                        pw.println("Desconocido");
                    }

                } catch (IOException |InterruptedException e) {
                    e.printStackTrace();
                }

                params.remove(args[i]);

            }


        } catch (IOException e) {
            e.printStackTrace();
        }



    }

}
