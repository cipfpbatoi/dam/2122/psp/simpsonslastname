import java.io.IOException;
import java.util.Scanner;

/**
 * 1. Realiza la clase Ejercicio1 ejecuta el proceso hijo en modo comando
 * pasándole como único Argumento Lenny. Utiliza para ellos los métodos de la
 * clase Process que nos permitirían mostrar el resultado de la ejecución
 * del comando tanto si se ha ejecutado correctamente como si ha habido
 * algún problema.
 *
 */
public class Ejercicio1 {

    public static void main(String[] args)  {

        String comando_texto = "java -jar SimpsonsDict.jar Lenn";

        ProcessBuilder pb = new ProcessBuilder(comando_texto.split(" "));
        pb.redirectErrorStream(true);

        try {

            Process lenny = pb.start();
            lenny.waitFor();

            Scanner leerSalidaDesdeLenny = new Scanner(lenny.getInputStream());

            while ( leerSalidaDesdeLenny.hasNextLine()){
                System.out.println(leerSalidaDesdeLenny.nextLine());
            }

        } catch (IOException|InterruptedException e){
            e.printStackTrace();
        }






    }

}
