import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Locale;
import java.util.Scanner;

/**
 * 3. Realiza la Clase Ejercicio3 que ejecute el proceso hijo en modo interactivo
 * sin argumentos y que formatee la salida de forma que le dé mas información
 * al usuario. Una posible forma de hacerlo seria :
 *      Introduce el Nombre del personaje de la serie:
 *      Si hemos podido encontrar el apellido del personaje, lo verás aquí:
 *      Introduce el Nombre...
 * ...
 *
 */
public class Ejercicio3 {

    public static void main(String[] args) {

        String comando_texto = "java -jar SimpsonsDict.jar";

        ProcessBuilder pb = new ProcessBuilder(comando_texto.split(" "));
        pb.redirectErrorStream(true);

        try {

            Process lenny = pb.start();

            //Leer desde el proceso Hijo
            Scanner leerSalidaDesdeLenny = new Scanner(lenny.getInputStream());

            //Escribir al proceso Hijo
            OutputStream os = lenny.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bfw = new BufferedWriter(osw);

            //Leer la entrada estándar del usuario en el Padre
            Scanner sc = new Scanner(System.in);

            System.out.print("Introduce el nombre del personaje de la serie: ");
            String linea = sc.next();

            while (!linea.toLowerCase().equals("stop")){
                bfw.write(linea);
                bfw.newLine();
                bfw.flush();
                System.out.printf("Si hemos podido traducir el nombre aparecerá aqui: %s",
                          leerSalidaDesdeLenny.nextLine());
                System.out.println();
                System.out.print("Introduce el nombre del personaje de la serie: ");
                linea = sc.next();
            }

        } catch (IOException e){
            e.printStackTrace();
        }







    }

}
